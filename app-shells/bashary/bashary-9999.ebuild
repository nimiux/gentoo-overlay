# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A bashed shell."
HOMEPAGE="http://github.com.lisper.es"
if [[ ${PV} == 9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://git.gentoo.es/proj/bashary.git"
	EGIT_REPO_URI="git+ssh://git@git.gentoo.es:1022/opt/scm/git/private/bashary.git"
	KEYWORDS=""
else
	SRC_URI="http://git.gentoo.es/dist/${PF}.tar.bz2"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="Unlicense"
SLOT="0"
IUSE=""

DEPEND=""
RDEPEND="
	app-shells/bash
	virtual/awk
	sys-apps/sed
	sys-fs/inotify-tools"
#app-doc/NaturalDocs"

RESTRICT="mirror"

src_install() {
	emake VERSION="${PV}" DESTDIR="${D}" install
}
