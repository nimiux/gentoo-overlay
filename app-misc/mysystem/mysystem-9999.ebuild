# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="My system"
HOMEPAGE="https://gitlab.com/nimiux"
SRC_URI=""

LICENSE=""
SLOT="0"
# TODO gitsync and eixsync are exclusive
IUSE="home gitsync eixsync mail moodle"

DEPEND=""
# TODO: Depend conditionally on cron
RDEPEND="app-shells/bashary
	virtual/cron"

S="${WORKDIR}"
LOGCRONDIR="/var/log/cron"

install_file() {
	local file="${1}"
	local dir="${2}"

	insinto "${dir}"
	doins "${FILESDIR}/${file}"
	sed -i -e "s/@@PACKAGE_NAME@@/${PN}/" "${D}/${dir}/$(basename ${file})" || die
}

install_cron_files() {
	local frequency="${1}"
	shift
	local files="${*}"

	for file in ${files} ; do
		install_file "cron/${file}" "/etc/cron.${frequency}"
		fperms 0750 "/etc/cron.${frequency}/${file}"
	done
}

install_backup_scripts() {
	install_cron_files daily backup-dirs clean-old-files status-report
}

install_home_scripts() {
	install_cron_files daily home-backup
}

install_moodle_scripts() {
	install_cron_files hourly moodle-cron
	install_cron_files daily moodle-backup moodlesite-backup
	install_cron_files weekly moodle-update
}

install_mail_scripts() {
	install_cron_files daily sa-updates
	# TODO: modify install_file to accept fperms as 3rd param
	install_file mail/spamchk /usr/local/bin
	fperms 0755 /usr/local/bin/spamchk
}

install_eixsync_scripts() {
	install_cron_files daily eix-sync
}

install_gitsync_scripts() {
	install_cron_files daily sync-portage
}

src_install() {
	dodir "${LOGCRONDIR}"
	# TODO create config file depending on modules (USE) installed
	newconfd "${FILESDIR}/config" "${PN}"
	install_backup_scripts
	use home && install_home_scripts
	use moodle && install_moodle_scripts
	use mail && install_mail_scripts
	use eixsync && install_eixsync_scripts
	use gitsync && install_gitsync_scripts
}
