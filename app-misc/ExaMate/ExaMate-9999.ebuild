# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

#DIST_AUTHOR=DROLSKY
#DIST_VERSION=0.56
inherit perl-module

DESCRIPTION="A colletion of functions"
HOMEPAGE="http://lisper.es"
EGIT_REPO_URI="https://git@gitlab.com/nimiux/examate"
EGIT_REPO_URI="git+ssh://git@gitlab.com/nimiux/examate.git"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=""
DEPEND="dev-perl/Archive-Extract
	dev-perl/File-ShareDir-Install
	dev-perl/List-All-Utils
	dev-perl/Net-SSH-Perl
	dev-perl/Test-Most"
