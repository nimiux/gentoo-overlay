# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

DESCRIPTION="Emulator of the CPC Amstrad microcomputer series"
HOMEPAGE=""
SRC_URI="http://www.cpc-emu.org/cpcemu-linux-x86-1.7.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
