# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="A blah blah blah"
HOMEPAGE="Find it"
SRC_URI="mirror://sourceforge/project/${PN}/${PN}/v${PV}/${P}.zip"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
