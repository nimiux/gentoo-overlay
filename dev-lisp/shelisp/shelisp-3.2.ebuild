# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3 versionator

SHELISP_BASE="http://dan.corlan.net/${PN}"
MY_V="$(get_major_version)_$(get_after_major_version)"

DESCRIPTION="Shelisp is a very short program that provides mechanisms for composing and running Unix shell commands"
HOMEPAGE="http://dan.corlan.net/shelisp"
SRC_URI="${SHELISP_BASE}/${PN}_${MY_V}.lisp -> ${PN}.lisp
	doc? ( ${SHELISP_BASE}/${PN}_manual_${MY_V}.pdf -> ${PN}.pdf )"

LICENSE="LLGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

S="${WORKDIR}"

src_unpack() {
	if use doc ; then
		cp "${DISTDIR}/${PN}.pdf" "${S}" || die
	fi
	cp "${FILESDIR}/${PN}.asd" "${FILESDIR}/package.lisp" "${DISTDIR}/${PN}.lisp" "${S}" \
		|| die
}

src_prepare() {
	sed -e "/(provide/c(in-package :${PN})" -i "${PN}.lisp"
}

src_install() {
	common-lisp-install-sources *.lisp
	common-lisp-install-asdf "${PN}"
	use doc && dodoc ${PN}.pdf
}
