(defsystem :shelisp
  :version "3.2"
  :licence "Lisp Lesser General Public License version 2"
  :description "Shelisp is a very short program that provides mechanisms for composing and running Unix shell commands"
  :components
  ((:file "package")
   (:file "shelisp" :depends-on ("package"))))
