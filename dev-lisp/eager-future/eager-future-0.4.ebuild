# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3

DESCRIPTION=""
HOMEPAGE="http://common-lisp.net/project/eager-future/"
SRC_URI="http://common-lisp.net/project/${PN}/release/${P}.tgz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
