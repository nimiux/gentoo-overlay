# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3

DESCRIPTION="ABLE is an open source Common Lisp editor for Mac, Linux and Windows"
HOMEPAGE="https://common-lisp.net/project/able/"
SRC_URI="https://common-lisp.net/project/${PN}/files/${P}.zip"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-lisp/ltk
	dev-lisp/cl-fad
	dev-lisp/trivial-gray-streams"

#src_install() {
	#common-lisp-install-sources -t all bench/
	#common-lisp-install-asdf ${PN}
	#dodoc README.md
	#use doc && dohtml index.html
#}
