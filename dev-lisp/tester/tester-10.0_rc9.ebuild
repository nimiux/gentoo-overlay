# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3

MY_RELEASE="release${PV}"

DESCRIPTION="A test harness for Common Lisp"
HOMEPAGE="https://github.com/franzinc/tester"
SRC_URI="https://github.com/franzinc/${PN}/archive/${MY_RELEASE}.tar.gz"

LICENSE="LLGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}/${PN}-${MY_RELEASE}"
src_prepare() {
	mv "${PN}.cl" "${PN}.lisp" || die
}

src_install() {
	common-lisp-install-sources *.lisp
	common-lisp-install-asdf
	dodoc README.md
}
