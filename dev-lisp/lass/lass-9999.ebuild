# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3 git-r3

DESCRIPTION="Lisp Augmented Style Sheets"
HOMEPAGE="https://shinmera.github.io/LASS/"
EGIT_REPO_URI="https://github.com/Shinmera/LASS.git"

LICENSE="Artistic-2"
SLOT="0"
IUSE=""

RDEPEND="đev-lisp/trivial-indent
	dev-lisp/trivial-mimes
	dev-lisp/cl-base64"
