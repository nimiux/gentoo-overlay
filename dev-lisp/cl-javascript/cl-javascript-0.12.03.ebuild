# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3

DESCRIPTION="CL-JavaScript allows you to add user scripting to your Common Lisp application"
HOMEPAGE="http://marijnhaverbeke.nl/cl-javascript/"
SRC_URI="http://marijnhaverbeke.nl/${PN}/${PF}.tgz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

RDEPEND="dev-lisp/cl-ppcre
	dev-lisp/parse-js"

src_install() {
	common-lisp-install-sources -t all bench/
	common-lisp-install-asdf ${PN}
	dodoc README.md
	use doc && dohtml index.html
}
