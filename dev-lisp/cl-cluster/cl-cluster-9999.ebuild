# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3 git-r3

DESCRIPTION="Eos is a drop-in replacement for the FiveAM Test Framework"
HOMEPAGE="http://repo.or.cz/w/cl-cluster.git"
EGIT_REPO_URI="git://repo.or.cz/cl-cluster.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-lisp/bordeaux-threads
	dev-lisp/cl-zmq
	dev-lisp/eager-future
	dev-lisp/split-sequence
	dev-lisp/trivial-garbage"
