# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3 git-r3

DESCRIPTION="A very simple library to allow indentation hints for SWANK."
HOMEPAGE="https://github.com/Shinmera/trivial-indent"
EGIT_REPO_URI="https://github.com/Shinmera/${PN}"

LICENSE="Artistic-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
