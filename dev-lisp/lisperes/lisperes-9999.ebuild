# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3 git-r3 user

DESCRIPTION="A website"
HOMEPAGE="http://lisper.es"
EGIT_REPO_URI="git+ssh://git@git.gentoo.es:1022/opt/scm/git/private/${PN}.git"

LICENSE="Unlicense"
SLOT="0"
IUSE=""

RDEPEND="app-misc/tmux
	dev-lisp/cl-who
	dev-lisp/css-lite
	dev-lisp/hunchentoot
	dev-lisp/linedit
	dev-lisp/parenscript
	dev-lisp/sbcl"

USERNAME="${PN}"

pkg_preinst() {
	local userhome="/home/${USERNAME}"
	local group="users"
	local logdir=$(grep "(defparameter \*log-dir\*" "${D}${CLSOURCEROOT}/${PN}/specials.lisp" | \
		sed -e 's/.*"\(.*\)\".*/\1/') || die
	local staticfilesdir=$(grep "(defparameter \*document-root\*" "${D}${CLSOURCEROOT}/${PN}/specials.lisp" | \
		sed -e 's/.*"\(.*\)\".*/\1/') || die

	echo "*********************************"
	echo "PENDING:"
	echo " * Use a function to extract logdir and staticfilesdir"
	echo " * Install static assets (the stuff in htdocs) under staticfilesdir"
	echo "*********************************"
	enewuser "${USERNAME}" -1 /bin/bash "${userhome}" "${group}"
	dodir "${logdir}"
	chown "${USERNAME}"."${group}" "${D}/${logdir}" "${userhome}"/.sbclrc || die
}

pkg_postinst() {
	einfo "To start the application run:"
	einfo "tmux new -d -s lisperes sbcl"
	einfo "as ${USERNAME}"
	einfo "You can control it by using slime or slimv"
}
