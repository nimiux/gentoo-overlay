# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3 git-r3

DESCRIPTION=""
HOMEPAGE="http://www.cliki.net/cl-zmq"
EGIT_REPO_URI="git://repo.or.cz/cl-zmq.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-lisp/cffi
	net-libs/zeromq"
