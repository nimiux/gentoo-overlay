# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit common-lisp-3 git-r3

DESCRIPTION="Tiny Common Lisp library to detect mime types in files."
HOMEPAGE="https://github.com/Shinmera/trivial-mimes"
EGIT_REPO_URI="https://github.com/Shinmera/${PN}"

LICENSE="Artistic-2"
SLOT="0"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
