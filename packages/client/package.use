app-crypt/pinentry gtk ncurses gnome-keyring
app-editors/emacs xft libxml2
app-editors/vim python ruby lua perl
app-emulation/qemu sdl sdl2 spice
app-emulation/virtualbox -doc
app-emulation/virt-manager gtk
dev-lang/perl ithreads
dev-libs/efl -doc fbcon gif gstreamer opengl physics pixman png sdl sound tiff v4l2
dev-libs/icu -doc
dev-libs/nss utils
dev-libs/openssl -bindist
dev-python/Babel -doc
dev-python/jinja -doc
dev-python/rdflib -doc
dev-python/pygments -doc
dev-python/pyopenssl -doc
dev-python/requests -doc
dev-python/sphinx -doc
dev-python/six -doc
dev-python/sphinxcontrib-websupport -doc
dev-python/urllib3 -doc
dev-python/cffi -doc
dev-python/whoosh -doc
dev-ruby/json -doc
dev-ruby/racc -doc
media-gfx/gimp pdf tiff
media-gfx/imagemagick tiff
media-gfx/sane-backends usb
media-gfx/xsane gimp ocr tiff
media-libs/elementary examples fbcon quicklaunch sdl
media-libs/gd truetype fontconfig png
media-libs/libcaca -doc
media-libs/libpng apng
media-libs/lv2 -doc
media-libs/sdl-mixer mad
media-plugins/gst-plugins-meta X a52 aac alsa cdda dts dv dvb dvd ffmpeg flac http jack lame libass libvisual mms modplug mp3 mpeg musepack ogg opus oss taglib theora v4l vaapi vcd vorbis vpx wavpack x264 xv xvid
media-sound/easytag vorbis
media-video/mplayer v4l libcaca
net-mail/mu emacs
net-mail/notmuch emacs mutt
net-misc/openssh -bindist
sys-apps/busybox mdev
sys-devel/gcc gcj
www-plugins/gnash nsplugin gtk
x11-libs/libXmu -doc
x11-wm/stumpwm doc contrib

app-eselect/eselect-php fpm
dev-lang/php bzip2 calendar cgi cli crypt ctype curl debug doc fastbuild flatfile fpm gd gmp hash intl ncurses nls pdo postgres readline reflection session soap sqlite ssl tokenizer unicode xml xmlreader xmlrpc xmlwriter xsl zip zlib

# required by media-libs/vigra-1.9.0::gentoo
# required by media-gfx/enblend-4.1.1::gentoo
# required by media-gfx/hugin-2014.0.0::gentoo
# required by hugin (argument)
>=dev-libs/boost-1.56.0-r1 python

# required by virtual/libgudev-215-r1[-systemd]
# required by sys-fs/udisks-2.1.3
# required by app-emulation/wine-1.6.2[udisks]
# required by @selected
# required by @world (argument)
=sys-fs/udev-216 gudev

# required by net-print/cups-2.0.2-r1::gentoo
# required by net-print/cups-filters-1.0.66::gentoo
>=app-text/ghostscript-gpl-9.15-r1 cups

# required by app-misc/muttprint-0.73-r3[doc]
# required by @selected
# required by @world (argument)
>=app-text/docbook-sgml-utils-0.6.14-r1 jadetex

# required by gnome-base/gnome-keyring-3.14.0
# required by gnome-base/libgnome-keyring-3.12.0
# required by gnome-base/libgnomeui-2.24.5-r1
# required by dev-python/libgnome-python-2.28.1-r1
# required by media-sound/soundconverter-2.1.5
# required by @selected
# required by @world (argument)
>=app-crypt/gcr-3.14.0 gtk

# required by app-emulation/virt-manager-1.1.0
# required by @selected
# required by @world (argument)
>=app-emulation/libvirt-glib-0.2.0 python

# required by sys-power/suspend-1.0::gentoo[crypt]
# required by suspend (argument)
>=dev-libs/libgpg-error-1.13 static-libs

# required by sys-power/suspend-1.0::gentoo
# required by suspend (argument)
# required by sys-power/suspend-1.0::gentoo[crypt]
# required by suspend (argument)
>=dev-libs/libgcrypt-1.5.4-r1 static-libs

# required by app-emulation/libvirt-1.2.18-r1::gentoo
# required by dev-python/libvirt-python-1.2.18::gentoo
# required by app-emulation/virt-manager-1.1.0-r2::gentoo
# required by @selected
# required by @world (argument)
>=net-firewall/iptables-1.4.21-r1 ipv6

# required by media-plugins/gst-plugins-meta-1.0-r2::gentoo
# required by gst-plugins-meta (argument)
>=media-libs/gst-plugins-base-1.4.5 theora

# required by media-sound/pulseaudio-6.0::gentoo[alsa-plugin,alsa]
# required by media-plugins/gst-plugins-pulse-1.4.5::gentoo
# required by media-plugins/gst-plugins-meta-1.0-r2::gentoo[pulseaudio]
# required by gst-plugins-meta (argument)
>=media-plugins/alsa-plugins-1.0.29 pulseaudio

# required by x11-libs/libxcb-1.11.1::gentoo[doc]
# required by x11-libs/libX11-1.6.3::gentoo
# required by x11-terms/xterm-320::gentoo
# required by @selected
# required by @world (argument)
>=app-doc/doxygen-1.8.13-r1 dot -doc latex

# required by media-gfx/gimp-2.9.2::gentoo
# required by @selected
# required by @world (argument)
>=media-libs/gegl-0.3.4 cairo

# required by www-client/chromium-48.0.2564.41::gentoo[system-ffmpeg]
# required by chromium (argument)
>=media-video/ffmpeg-2.8.4 opus vpx theora gnutls

# required by dev-python/certifi-2015.11.20::gentoo
#>=dev-python/setuptools-18.4 python_targets_python3_3

# required by dev-python/setuptools-18.4::gentoo
# required by www-client/pybugz-0.12.1::gentoo
# required by app-portage/tatt-0.3::gentoo
# required by @selected
# required by @world (argument)
#>=dev-python/certifi-2015.11.20 python_targets_python3_3

# required by dev-python/setuptools-18.4::gentoo
# required by dev-python/certifi-2015.11.20::gentoo
#>=dev-python/packaging-15.3-r2 python_targets_python3_3

# required by media-gfx/inkscape-0.91-r1::gentoo
# required by @selected
# required by @world (argument)
>=app-text/poppler-0.42.0 cairo

# required by dev-lisp/ecls-13.5.1-r1::gentoo
# required by ecls (argument)
>=dev-libs/boehm-gc-7.4.2 threads

# required by media-gfx/gimp-2.9.4::gentoo
# required by @selected
# required by @world (argument)
>=media-libs/libmypaint-1.3.0_beta1 gegl

# required by x11-libs/gtk+-2.24.30::gentoo
# required by app-crypt/pinentry-0.9.5::gentoo
# required by app-crypt/gnupg-2.0.28::gentoo
# required by dev-vcs/git-2.7.3-r1::gentoo[gpg]
# required by @selected
# required by @world (argument)
>=x11-libs/gdk-pixbuf-2.32.3 X

# required by x11-libs/gtk+-2.24.30::gentoo
# required by app-crypt/pinentry-0.9.5::gentoo
# required by app-crypt/gnupg-2.0.28::gentoo
# required by dev-vcs/git-2.7.3-r1::gentoo[gpg]
# required by @selected
# required by @world (argument)
>=x11-libs/cairo-1.14.6 X

# required by sys-power/suspend-1.0_p20150810::gentoo
# required by @selected
# required by @world (argument)
>=dev-libs/lzo-2.08 static-libs

# required by dev-qt/qtgui-5.5.1-r1::gentoo[xcb]
# required by @preserved-rebuild (argument)
>=x11-libs/libxcb-1.11.1 xkb

# required by app-emulation/qemu-2.5.1::gentoo[-qemu_softmmu_targets_m68k,-qemu_softmmu_targets_ppcemb,-qemu_softmmu_targets_microblaze,-qemu_softmmu_targets_mips,-qemu_softmmu_targets_alpha,-qemu_softmmu_targets_or32,-qemu_softmmu_targets_sh4eb,-qemu_softmmu_targets_unicore32,-qemu_softmmu_targets_aarch64,-qemu_softmmu_targets_mips64,-qemu_softmmu_targets_s390x,qemu_softmmu_targets_i386,-qemu_softmmu_targets_mips64el,qemu_softmmu_targets_ppc64,-qemu_softmmu_targets_sparc,-qemu_softmmu_targets_xtensaeb,-qemu_softmmu_targets_tricore,qemu_softmmu_targets_ppc,qemu_softmmu_targets_x86_64,-qemu_softmmu_targets_microblazeel,-qemu_softmmu_targets_mipsel,-qemu_softmmu_targets_arm,opengl,-qemu_softmmu_targets_cris,-qemu_softmmu_targets_moxie,-qemu_softmmu_targets_sparc64,-qemu_softmmu_targets_lm32,-qemu_softmmu_targets_xtensa,-static-softmmu,-qemu_softmmu_targets_sh4]
# required by @selected
# required by @world (argument)
>=media-libs/mesa-11.0.6 gles2

# required by www-client/chromium-57.0.2987.98::gentoo[system-libvpx]
# required by @selected
# required by @world (argument)
>=media-libs/libvpx-1.5.0 svc postproc

# required by x11-drivers/xf86-video-ati-7.7.0::gentoo[glamor]
# required by x11-base/xorg-drivers-1.18-r1::gentoo[video_cards_radeon]
>=x11-base/xorg-server-1.18.4 glamor

# required by x11-misc/pcmanfm-1.2.3::gentoo
# # required by @selected
# # required by @world (argument)
>=x11-libs/libfm-1.2.4 gtk

# required by app-editors/neovim-0.1.7::gentoo
# required by neovim (argument)
>=dev-lua/mpack-1.0.4 luajit

# required by app-editors/neovim-0.1.7::gentoo
# required by neovim (argument)
>=dev-lua/lpeg-1.0.1 luajit

# required by dev-qt/qtwebengine-5.6.2::gentoo
# required by sci-geosciences/gpsbabel-1.5.4::gentoo
# required by @selected
# required by @world (argument)
>=dev-qt/qtwebchannel-5.6.2 qml

# required by sci-geosciences/gpsbabel-1.5.4::gentoo
# required by @selected
# required by @world (argument)
>=dev-qt/qtwebengine-5.6.2 widgets

# required by dev-texlive/texlive-xetex-2015::gentoo
# required by dev-texlive/texlive-formatsextra-2015::gentoo
# required by app-text/texlive-2015::gentoo[extra]
# required by app-doc/doxygen-1.8.13-r1::gentoo[latex]
# required by x11-libs/libXi-1.7.9::gentoo[doc]
# required by x11-drivers/xf86-input-synaptics-1.9.0::gentoo
# required by x11-base/xorg-drivers-1.19::gentoo[input_devices_synaptics]
# required by x11-base/xorg-server-1.19.2::gentoo[xorg]
# required by x11-drivers/xf86-input-evdev-2.10.5::gentoo
>=app-text/texlive-core-2015-r1 xetex

# required by app-doc/doxygen-1.8.13-r1::gentoo[latex]
# required by x11-libs/libXi-1.7.9::gentoo[doc]
# required by x11-drivers/xf86-input-synaptics-1.9.0::gentoo
# required by x11-base/xorg-drivers-1.19::gentoo[input_devices_synaptics]
# required by x11-base/xorg-server-1.19.2::gentoo[xorg]
# required by x11-drivers/xf86-input-evdev-2.10.5::gentoo
>=app-text/texlive-2015 extra

# required by net-mail/notmuch-0.23.3::gentoo[crypt]
# required by notmuch (argument)
>=dev-libs/gmime-2.6.23 smime

# required by dev-util/meld-3.16.4::gentoo
# required by meld (argument)
>=dev-libs/glib-2.50.3-r1 dbus

# KTurtle
# required by kde-frameworks/kcoreaddons-5.29.0::gentoo
# required by kde-frameworks/kconfigwidgets-5.29.0::gentoo
# required by kde-frameworks/kxmlgui-5.29.0::gentoo
# required by kde-apps/kturtle-16.12.3::gentoo
# required by kturtle (argument)
>=dev-qt/qtcore-5.6.2-r1 icu
# required by sys-auth/polkit-0.113::gentoo[-systemd]
# required by sys-fs/udisks-2.1.8::gentoo
# required by kde-frameworks/solid-5.29.0::gentoo
# required by kde-frameworks/kio-5.29.0-r1::gentoo
# required by kde-apps/kturtle-16.12.3::gentoo
# required by kturtle (argument)
>=sys-auth/consolekit-1.1.0-r1 policykit
# required by media-libs/phonon-4.9.0::gentoo[vlc]
# required by kde-frameworks/knotifications-5.29.0::gentoo
# required by kde-frameworks/kwallet-5.29.0::gentoo
# required by kde-frameworks/kio-5.29.0-r1::gentoo[kwallet]
# required by kde-apps/kturtle-16.12.3::gentoo
# required by kturtle (argument)
>=media-libs/phonon-vlc-0.9.0 qt5
# required by kde-plasma/polkit-kde-agent-5.8.6::gentoo
# required by kde-frameworks/kauth-5.29.0::gentoo[policykit]
# required by kde-frameworks/kconfigwidgets-5.29.0::gentoo
# required by kde-frameworks/kxmlgui-5.29.0::gentoo
# required by kde-apps/kturtle-16.12.3::gentoo
# required by kturtle (argument)
>=sys-auth/polkit-qt-0.112.0-r1 qt5
# required by media-libs/phonon-vlc-0.9.0::gentoo
>=media-libs/phonon-4.9.0 qt5
# required by media-libs/phonon-vlc-0.9.0::gentoo
# required by media-libs/phonon-4.9.0::gentoo[vlc]
# required by kde-frameworks/knotifications-5.29.0::gentoo
# required by kde-frameworks/kwallet-5.29.0::gentoo
# required by kde-frameworks/kio-5.29.0-r1::gentoo[kwallet]
# required by kde-apps/kturtle-16.12.3::gentoo
# required by kturtle (argument)
>=media-video/vlc-2.2.4-r1 dbus
# KTurtle


# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[doc,ruby_targets_ruby22]
>=dev-ruby/yard-0.8.7.3 ruby_targets_ruby22
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[doc,ruby_targets_ruby22]
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby21]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
>=dev-ruby/minitest-5.8.4 ruby_targets_ruby22
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[doc,ruby_targets_ruby22]
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby21]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-lang/ruby-2.1.9::gentoo
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby21]
# required by dev-ruby/power_assert-0.3.1::gentoo[-test,ruby_targets_ruby21]
>=dev-ruby/racc-1.4.11 ruby_targets_ruby22
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[doc,ruby_targets_ruby22]
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby21]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
>=dev-ruby/rake-10.5.0 ruby_targets_ruby22
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.2.0::gentoo
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[doc,ruby_targets_ruby22]
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby21]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-lang/ruby-2.1.9::gentoo
# required by dev-ruby/power_assert-0.3.1::gentoo[ruby_targets_ruby21]
>=dev-ruby/rubygems-2.5.2 ruby_targets_ruby22
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[ruby_targets_ruby22]
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby21]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/power_assert-0.3.1::gentoo[doc,ruby_targets_ruby21]
>=dev-ruby/hoe-3.13.0-r1 ruby_targets_ruby22
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[doc,ruby_targets_ruby22]
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby21]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.6::gentoo
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/power_assert-0.3.1::gentoo[-test,ruby_targets_ruby21]
>=dev-ruby/json-1.8.3 ruby_targets_ruby22
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[doc,ruby_targets_ruby22]
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby21]
>=dev-ruby/test-unit-3.1.9 ruby_targets_ruby22
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[doc,ruby_targets_ruby22]
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby21]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
>=dev-ruby/power_assert-0.3.1 ruby_targets_ruby22
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby22]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/power_assert-0.3.1::gentoo[doc,ruby_targets_ruby21]
>=dev-ruby/bluecloth-2.2.0-r3 ruby_targets_ruby22
# required by dev-ruby/yard-0.8.7.3::gentoo[-test,ruby_targets_ruby22]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.6::gentoo
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.2.0::gentoo[ruby_targets_ruby22]
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[doc,ruby_targets_ruby22]
>=virtual/rubygems-11 ruby_targets_ruby22
# required by dev-lang/ruby-2.2.6::gentoo[rdoc]
# required by dev-ruby/json-1.8.3::gentoo[ruby_targets_ruby22]
# required by dev-lang/ruby-2.1.9::gentoo
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby21]
# required by dev-ruby/bluecloth-2.2.0-r3::gentoo[-test,ruby_targets_ruby21]
# required by dev-ruby/yard-0.8.7.3::gentoo[doc,ruby_targets_ruby21]
# required by dev-ruby/test-unit-3.1.9::gentoo[doc,ruby_targets_ruby22]
>=dev-ruby/rdoc-4.2.0 ruby_targets_ruby22

# required by sci-mathematics/wxmaxima-15.08.2::gentoo
# required by wxmaxima (argument)
>=sci-visualization/gnuplot-5.0.1-r1 wxwidgets cairo

# required by app-office/libreoffice-5.4.2.2::gentoo
# required by @selected
# required by @world (argument)
>=dev-libs/xmlsec-1.2.24-r1 nss

# required by dev-db/sqliteman-1.2.2-r4::gentoo
# required by @selected
# required by @world (argument)
>=x11-libs/qscintilla-2.10.1 qt5

# required by x11-misc/xdg-utils-1.1.1-r1::gentoo
# required by kde-frameworks/kf-env-4::gentoo
# required by kde-frameworks/kauth-5.37.0::gentoo
# required by kde-frameworks/kconfigwidgets-5.37.0::gentoo
# required by kde-frameworks/kiconthemes-5.37.0::gentoo
# required by kde-frameworks/knewstuff-5.37.0::gentoo
# required by kde-apps/kturtle-17.08.3::gentoo
# required by @selected
# required by @world (argument)
>=app-text/xmlto-0.0.26-r1 text

>=dev-libs/gobject-introspection-1.34 python_single_target_python3_6

=app-text/asciidoc-8.6.10 python_single_target_python2_7


# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.3.0::gentoo[-test,ruby_targets_ruby23]
# required by dev-ruby/minitest-5.9.1::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.9::gentoo
# required by dev-ruby/net-telnet-0.1.1-r1::gentoo[ruby_targets_ruby22]
>=dev-ruby/did_you_mean-1.0.2 ruby_targets_ruby23
# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.3.0::gentoo[-test,ruby_targets_ruby23]
# required by dev-ruby/net-telnet-0.1.1-r1::gentoo[doc,ruby_targets_ruby22]
>=dev-ruby/minitest-5.9.1 ruby_targets_ruby23
# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.3.0::gentoo[-test,ruby_targets_ruby23]
# required by dev-ruby/minitest-5.9.1::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.9::gentoo
# required by dev-ruby/power_assert-0.3.1::gentoo[ruby_targets_ruby22]
# required by dev-ruby/test-unit-3.1.9::gentoo[ruby_targets_ruby23]
>=dev-ruby/net-telnet-0.1.1-r1 ruby_targets_ruby23
# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.3.0::gentoo[-test,ruby_targets_ruby23]
# required by dev-ruby/minitest-5.9.1::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.9::gentoo
# required by dev-ruby/net-telnet-0.1.1-r1::gentoo[ruby_targets_ruby22]
>=dev-ruby/power_assert-0.3.1 ruby_targets_ruby23
# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.3.0::gentoo[-test,ruby_targets_ruby23]
# required by dev-ruby/minitest-5.9.1::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.9::gentoo
# required by dev-ruby/net-telnet-0.1.1-r1::gentoo[ruby_targets_ruby22]
>=dev-ruby/rake-10.5.0 ruby_targets_ruby23
# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/rdoc-4.3.0::gentoo[-test,ruby_targets_ruby23]
# required by dev-ruby/minitest-5.9.1::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.2.9::gentoo
# required by dev-ruby/net-telnet-0.1.1-r1::gentoo[ruby_targets_ruby22]
>=dev-ruby/test-unit-3.1.9 ruby_targets_ruby23
# required by dev-ruby/rdoc-4.3.0::gentoo[ruby_targets_ruby23]
# required by dev-ruby/minitest-5.9.1::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/racc-1.4.14::gentoo[-test,ruby_targets_ruby23]
>=dev-ruby/json-1.8.6-r1 ruby_targets_ruby23
# required by dev-lang/ruby-2.3.6::gentoo[rdoc]
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/racc-1.4.14::gentoo[-test,ruby_targets_ruby23]
>=dev-ruby/rdoc-4.3.0 ruby_targets_ruby23
# required by dev-ruby/rdoc-4.3.0::gentoo[ruby_targets_ruby23]
# required by dev-ruby/minitest-5.9.1::gentoo[doc,ruby_targets_ruby22]
# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby22]
# required by dev-ruby/json-1.8.6-r1::gentoo
# required by dev-lang/ruby-2.2.9::gentoo
# required by dev-ruby/net-telnet-0.1.1-r1::gentoo[ruby_targets_ruby22]
>=dev-ruby/racc-1.4.14 ruby_targets_ruby23
# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/rubygems-2.6.14::gentoo[ruby_targets_ruby23]
>=virtual/rubygems-11 ruby_targets_ruby23
# required by virtual/rubygems-11::gentoo[ruby_targets_ruby23]
# required by dev-lang/ruby-2.3.6::gentoo
# required by dev-ruby/did_you_mean-1.0.2::gentoo[ruby_targets_ruby23]
>=dev-ruby/rubygems-2.6.14 ruby_targets_ruby23

# required by app-emulation/virt-manager-1.4.3::gentoo[gtk]
# required by @selected
# required by @world (argument)
>=net-libs/gtk-vnc-0.7.1 python
# required by app-emulation/virt-manager-1.4.3::gentoo[gtk]
# required by @selected
# required by @world (argument)
>=net-misc/spice-gtk-0.33-r2 usbredir gtk3

# required by dev-python/reportlab-3.3.0-r2::gentoo
# required by dev-python/rst2pdf-0.93-r3::gentoo
# required by media-video/mpv-0.27.2::gentoo[doc]
# required by mpv (argument)
>=dev-python/pillow-3.4.2-r1 tiff
# required by dev-qt/qtcore-5.9.4-r2::gentoo
# required by dev-qt/qtwebchannel-5.9.4::gentoo
# required by dev-qt/qtwebengine-5.9.4::gentoo
# required by sci-geosciences/gpsbabel-1.5.4-r1::gentoo[gui]
# required by @selected
# required by @world (argument)
>=dev-libs/libpcre2-10.30 pcre16 pcre32

# required by dev-qt/qtcore-5.5.1-r1::gentoo
# required by dev-qt/qtgui-5.5.1-r1::gentoo
# required by @preserved-rebuild (argument)
>=dev-libs/libpcre-8.38-r1 pcre16

# required by dev-python/sqlalchemy-1.2.4::gentoo[python_targets_python3_6]
# required by dev-python/sphinxcontrib-websupport-1.0.1-r1::gentoo
# required by dev-python/sphinx-1.6.5::gentoo[-test]
# required by kde-frameworks/extra-cmake-modules-5.43.0::gentoo[doc]
# required by kde-frameworks/breeze-icons-5.43.0::gentoo
# required by kde-apps/kturtle-17.12.3::gentoo
# required by @selected
# required by @world (argument)
>=dev-lang/python-3.6.5 sqlite
# required by sys-fs/udev-225::gentoo
# required by @selected
# required by @world (argument)
>=dev-lang/python-2.7.10-r1:2.7 sqlite
=dev-lang/python-3.6.3-r1 sqlite

# required by sys-kernel/genkernel-3.5.3.3::gentoo
# required by @selected
# required by @world (argument)
>=sys-apps/util-linux-2.30.2-r1 static-libs


# required by www-client/chromium-71.0.3578.80::gentoo
# required by chromium (argument)
>=dev-libs/libxml2-2.9.8 icu python

# required by www-client/chromium-71.0.3578.80::gentoo
# required by chromium (argument)
>=media-libs/harfbuzz-2.0.2 icu
# required by www-client/chromium-71.0.3578.80::gentoo
# required by chromium (argument)
>=sys-libs/zlib-1.2.11-r2 minizip
# required by www-client/chromium-71.0.3578.80::gentoo
# required by chromium (argument)
>=net-libs/nodejs-8.14.0 -doc inspector
